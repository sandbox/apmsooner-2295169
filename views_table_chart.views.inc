<?php
/**
 * @file
 * Views-specific implementations and functions.
 */

/**
 * Implements hook_views_plugins().
 */
function views_table_chart_views_plugins() {
  $plugins = array(
    'style' => array(
      'table_chart' => array(
        'title' => t('Table Chart'),
        'help' => t('Display table as chart.'),
        'handler' => 'views_table_chart_plugin_style',
        'theme' => 'views_view_table',
        'uses row plugin' => TRUE,
        'uses fields' => TRUE,
        'uses options' => TRUE,
        'uses grouping' => FALSE,
        'type' => 'normal',
        'parent' => 'table',
      ),
    ),
  );

  libraries_load('highcharts');
  libraries_load('highcharttable');
  drupal_add_js(drupal_get_path('module', 'views_table_chart') . '/js/views_table_chart.js');
  return $plugins;
}
