<?php
/**
 * @file
 * Contains the Table Chart style plugin.
 */

/**
 * Style plugin to render view as a chart.
 *
 * @ingroup views_style_plugins
 */
class views_table_chart_plugin_style extends views_plugin_style_table {

  function init(&$view, &$display, $options = NULL) {
    parent::init($view, $display, $options);
  }
/**
 * Set default options.
 */
  function options(&$options) {
    $options['chart_type'] = array('default' => 'column');
    $options['datalabels_enabled'] = array('default' => TRUE);
    $options['legend_disabled'] = array('default' => FALSE);
    $options['legend_layout'] = array('default' => 'horizontal');
    return $options;
  }
/**
 * Create forms to hold these values allowing the user to change the values.
 */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['chart_options'] = array(
      '#type' => 'fieldset',
      '#title' => t('Chart Options'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    $form['chart_type'] = array(
      '#type' => 'select',
      '#title' => t('Chart Type'),
      '#options' => array(
        'column' => t('Column'),
        'line' => t('Line'),
        'area' => t('Area'),
        'spline' => t('Spline'),
        'pie' => t('Pie'),
      ),
      '#description' => t('This field determines the type of chart that will display.'),
      '#default_value' => $this->options['chart_type'],
      '#fieldset' => 'chart_options',
    );
    $form['datalabels_enabled'] = array(
      '#type' => 'checkbox',
      '#title' => t('Display Chart Labels'),
      '#description' => t('Displays values on each point of the graph.'),
      '#default_value' => $this->options['chart_labels_enabled'],
      '#fieldset' => 'chart_options',
    );
    $form['chart_legend'] = array(
      '#type' => 'fieldset',
      '#title' => t('Chart Legend'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    $form['legend_disabled'] = array(
      '#type' => 'checkbox',
      '#title' => t('Disable the legend'),
      '#default_value' => $this->options['legend_disabled'],
      '#fieldset' => 'chart_legend',
    );
    $form['legend_layout'] = array(
      '#type' => 'select',
      '#title' => t('Legend Layout'),
      '#default_value' => $this->options['legend_layout'],
      '#options' => array(
        'horizontal' => t('Horizontal'),
        'vertical' => t('Vertical'),
      ),
      '#fieldset' => 'chart_legend',
    );
  }
}
