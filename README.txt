
Views Table Chart
============

Installation
============

Module Dependencies

    Views 3.x
    Libraries API 2.x

Third party Libraries

    Highcharts (see licensing @ http://shop.highsoft.com/highcharts.html)
      - http://www.highcharts.com/download
    Highcharts Table
      - https://github.com/highchartTable/jquery-highchartTable-plugin

Instructions for usage

    1. Download Highcharts library, unpack and rename to highcharts and move to
       sites/all/libraries. Example: sites/all/libraries/highcharts
    2. Download Highcharts Table library, unpack and rename to highcharttable
       and move to sites/all/libraries. Eg: sites/all/libraries/highcharttable
    3. Install and enable Views Table Chart module.
    4. Create a view of fields with labels specified.
    5. Select Table Chart as the style under format settings in the view.
    6. Select the type of chart you want to create. Default is Column.
    7. Save your view and render the page, block etc...
    8. Thats it! you should now have a chart.

Note: The table itself does not have to be viewable for the chart to work so if
you only want the chart to appear without the underlying table, just add
table.highchart {display: none;} to your stylesheet.
