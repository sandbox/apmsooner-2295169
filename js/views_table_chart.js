/**
 * @file views_table_chart.js
 *
 */

(function ($, Drupal) {

    Drupal.behaviors.chartTable = {
        attach: function (context, settings) {

$('table.highchart', context).once('table-chart').highchartTable();

        }
    };

})(jQuery, Drupal);

units = {
    percentCallback : function (value) {
        return value + '%'
    }
}
